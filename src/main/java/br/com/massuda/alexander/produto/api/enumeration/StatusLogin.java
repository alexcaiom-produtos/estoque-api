package br.com.massuda.alexander.produto.api.enumeration;

/**
 * @author Alex
 *
 */
public enum StatusLogin {

	NORMAL, BLOQUEADO;
	
}
