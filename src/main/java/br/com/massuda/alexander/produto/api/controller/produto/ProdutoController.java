/**
 * 
 */
package br.com.massuda.alexander.produto.api.controller.produto;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.produto.api.dao.DAO;
import br.com.massuda.alexander.produto.api.dao.Finder;
import br.com.massuda.alexander.produto.api.mapper.MapperUsuario;
import br.com.massuda.alexander.produto.api.model.produto.Produto;
import br.com.massuda.alexander.produto.api.servico.ServicoUsuario;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;
import br.com.waiso.framework.exceptions.ErroUsuario;

/**
 * @author Alex
 *
 */
@Controller
@RequestMapping("/produto")
public class ProdutoController {
	
	@Autowired
	ServicoUsuario servicoUsuario;
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@Valid @RequestBody Produto o) throws ErroUsuario, SQLException {
		DAO<Produto> dao = new DAO<Produto>(Produto.class) {};
		
		Usuario responsavel = servicoUsuario.pesquisarPorId(o.getResponsavel().getId());//servicoUsuario.pesquisarPorLogin(tarefa.getCriador().getLogin());
		o.setResponsavel(MapperUsuario.to(responsavel));
		o.setCriacao(LocalDateTime.now());
		dao.incluir(o);
	}
	
	@PutMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void editar(@Valid @RequestBody Produto o) throws ErroUsuario, SQLException {
		DAO<Produto> dao = new DAO<Produto>(Produto.class) {};
		
		Usuario responsavel = servicoUsuario.pesquisarPorId(o.getResponsavel().getId());//servicoUsuario.pesquisarPorLogin(tarefa.getCriador().getLogin());
		o.setResponsavel(MapperUsuario.to(responsavel));
		o.setCriacao(LocalDateTime.now());
		dao.editar(o);
	}
	
	@ResponseBody
	@GetMapping
	public List<Produto> listar() {
		Finder<Produto> finder = new Finder<Produto>(Produto.class) {};
		
		List<Produto> lista = finder.listar();
		for (int indice = 0; indice < lista.size(); indice++) {
			Usuario responsavel = null;
			if (ObjectUtils.isEmpty(lista.get(indice).getResponsavel())) {
				responsavel = servicoUsuario.pesquisarPorId(lista.get(indice).getResponsavel().getId());
			}
			lista.get(indice).setResponsavel(MapperUsuario.from(responsavel));
		}
		
		return lista;
	}
	
	@ResponseBody
	@GetMapping("/{id}")
	public Produto pesquisarTarefa(@PathVariable long id) {
		Finder<Produto> finder = new Finder<Produto>(Produto.class) {};
		Produto o = finder.pesquisar(id);
		br.com.massuda.alexander.produto.api.model.Usuario responsavel = MapperUsuario.from(servicoUsuario.pesquisarPorId(o.getResponsavel().getId()));
		o.setResponsavel(responsavel);
		return o;
	}
	
	@ResponseBody
	@GetMapping("/usuario/{usuario}")
	public List<Produto> listarTarefa(@PathVariable long usuario, LocalDateTime inicio, LocalDateTime fim) {
		Finder<Produto> finder = new Finder<Produto>(Produto.class) {};
		Filtro f = new Filtro(Long.class, "usuario_id", TipoFiltro.IGUAL_A, usuario, true);
		return finder.pesquisar(Arrays.asList(f));
	}
	
}
