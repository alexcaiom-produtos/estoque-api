/**
 * 
 */
package br.com.massuda.alexander.produto.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

/**
 * @author Alex
 *
 */
@Controller
@RequestMapping("/index")
public class IndexController extends ControllerBase {

	@CrossOrigin
	@ResponseBody
	@GetMapping("/linkAreaLogada")
	public String getLinkAreaLogada (String usuario) {
//		boolean estaLogado = new RestTemplate().getForObject("http://alexcaiom.ddns.net:83/crm-api/login/estaLogado?usuario="+usuario, Boolean.class);
		boolean estaLogado = new RestTemplate().getForObject("http://localhost:83/crm-api/login/estaLogado?usuario="+usuario, Boolean.class);
		return estaLogado ? "/area-logada/home.html" : "/login_v2.html";
	}
	
	
}
