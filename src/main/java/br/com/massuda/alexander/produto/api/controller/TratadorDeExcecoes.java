package br.com.massuda.alexander.produto.api.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;

import br.com.massuda.alexander.spring.framework.infra.excecoes.Erro;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class TratadorDeExcecoes extends br.com.massuda.alexander.spring.framework.infra.web.excecoes.tratamento.TratadorDeExcecoes {
	
//	@ExceptionHandler({RestClientException.class, Erro.class, Exception.class})
//	public ResponseEntity<String> tratarErroHTTP(Exception e) throws Erro {
//		LOGGER.error("Erro:", e);
//		return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
//	}
	
}
