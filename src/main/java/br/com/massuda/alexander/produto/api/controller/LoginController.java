/**
 * 
 */
package br.com.massuda.alexander.produto.api.controller;

import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.massuda.alexander.produto.api.framework.model.SessaoDeUsuario;
import br.com.massuda.alexander.produto.api.servico.ServicoUsuario;
import br.com.massuda.alexander.produto.api.utils.GerenciadorDeSessoes;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

/**
 * @author Alex
 *
 */
@RestController
@RequestMapping("/login")
public class LoginController extends ControllerBase {

	@Autowired
	protected GerenciadorDeSessoes gerenciador;
	@Autowired
	private ServicoUsuario servico;
	
	
	@CrossOrigin
	@ResponseBody
	@GetMapping("/estaLogado")
	public boolean estaLogado(String usuario) {
		return gerenciador.getStatusLogon(usuario);
	}
	
	@CrossOrigin
	@ResponseBody
	@PostMapping("/deslogar")
	public void deslogar(String usuario) {
		boolean estaLogado = estaLogado(usuario);
		if (estaLogado) {
			gerenciador.removeSessaoUsuario(usuario);
		}
	}
	
	@CrossOrigin
	@ResponseBody
	@PostMapping(value="/logar")
	public String logar (String usuario, String senha) throws Erro {
		Usuario usuarioAutenticado = servico.autenticar(usuario, senha);
		SessaoDeUsuario sessaoDeUsuario = new SessaoDeUsuario();
		sessaoDeUsuario.setUsuario(new br.com.massuda.alexander.produto.api.model.Usuario(usuarioAutenticado));
		sessaoDeUsuario.setHorarioLogin(GregorianCalendar.getInstance());
		gerenciador.addSessaoUsuario(usuario, sessaoDeUsuario);
		return usuario;
	}
	
}
